<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function () {
    return ('Hello Laravel');
});

Route::get('/student/{id}', function ($id = 'No student found') {
    return ('We got student with id: '.$id);
});

Route::get('/car/{id?}', function ($id = null) {
    if(isset($id)){
        //TODO: validation for integer
        return "We got car id: $id";
    }else{
        return 'Please insert the id to find your car';
    }
});

Route::get('/comment/{id}', function ($id) {
    return view('comment', compact('id'));
});


##Exercise  5

// Route::get('/users/{email}/{name?}', function ($email, $name = null) {
//     if(filter_var($email, FILTER_VALIDATE_EMAIL)&& $name != null){
//         return view('users', compact('email','name'));

//     }elseif (filter_var($email, FILTER_VALIDATE_EMAIL)&& $name == null){
//         $name ='name is missing';
//         return view('users', compact('email', 'name'));
    
//     }elseif($name==null) {
//         return 'Error: invalid email, enter valid email!';
//     }
//    elseif(filter_var($name, FILTER_VALIDATE_EMAIL)) {
//        $tempName=$email;
//        $email=$name;
//        $name=$tempName;
//        return view('users', compact('email','name'));
//    }else {
//     return 'Error: invalid email, enter valid email!';
//     }
   
// });

//Lesson6 + 8(middleware) - middlewere - just registered users can make this change

Route::resource('candidates', 'CandidatesController') -> middleware('auth');
Route::get('/users', 'UsersController@index')-> name('users');

//lesson 8

Route::get('candidates/delete/{id}', 'CandidatesController@destroy') -> name('candidates.delete');
Route::get('auth/delete/{id}', 'UsersController@destroy') -> name('auth.delete');
Route::get('auth/edit/{id}', 'UsersController@edit') -> name('auth.edit');
Route::post('auth/update{id}', 'UsersController@update')->name('auth.update');


//lesson 9

Route::get('candidates/changeuser/{cid}/{uid?}', 'CandidatesController@changeUser') -> name('candidates.changeuser')-> middleware('auth');;

Route::get('candidates/changestatus/{cid}/{sid?}', 'CandidatesController@changeStatus') -> name('candidates.changestatus')-> middleware('auth');

Route::get('candidates/changedepartment/{cid}/{did?}', 'CandidatesController@changeDepartment') -> name('candidates.changedepartment')-> middleware('auth');

Route::get('mycandidates', 'CandidatesController@mycandidates') -> name('candidates.mycandidates');


        
   

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('candidates/details/{id}', 'CandidatesController@details') -> name('candidates.details');
