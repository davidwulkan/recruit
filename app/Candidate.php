<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Candidate extends Model
{
    use Sortable;

    protected $fillable = ['name', 'email'];

    //Relationships
    public function owner(){
        //One to many - every candidate has ONE user
        return $this->belongsTo('App\User', 'user_id');
    }

    public function status(){
        return $this->belongsTo('App\status');
    }

    public function department(){
        return $this->belongsTo('App\Department');
    }
    


}

