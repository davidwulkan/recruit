<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class status extends Model
{
    protected $fillable =['name'];
    public function candidates(){
    
        return $this->hasMany('App\candidate');
    } 

    public static function next($status_id){
        
        //goes to nextstages table where there is from to a parameter status_id and returns the "to" result of the relevant status_id
        $nextsages = DB::table('nextstages')->where('from', $status_id)->pluck('to');
        return self::find($nextsages)->all();
    }

    public static function allowed($from,$to){
                $allowed = DB::table('nextstages')->where('from',$from)->where('to',$to)->get();
                if(isset($allowed)) return true;
                return false;
    }
    

    

    


}