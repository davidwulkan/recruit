@extends('layouts.app')

@section ('title', 'Candidates')

@section('content')

@if(Session::has('notallowed'))
    <div class = 'alert alert-danger'>
        {{Session::get('notallowed')}}
    </div>
@endif

<div class="jumbotron text-center">
<h1>List of candidates</h1>
<a href="{{url('/candidates/create')}}" class="btn btn-primary">Add new candidate</a>
</div>
<table class = "table table-striped">
<thead class="thead-dark">
    <tr>
        <th>ID</th><th>Name</th><th>Email</th><th>@sortablelink('Age')</th><th>Owner</th><th>Status</th><th>Created</th><th>Updated</th><th></th><th></th>
    </tr>
</thead>
    <!-- the table data -->
    
    @foreach ($candidates as $candidate)
    @if($candidate->status->name == 'accepted to work')
        <tr style = "color:green">
    @elseif ($candidate->status->name == 'not fit' || $candidate->status->name == 'not fit professionally')
        <tr style = "color:red">
    @else
    <tr>
    @endif
        
            <td>{{$candidate->id}}</td>
            <td>{{$candidate->name}}</td>
            <td>{{$candidate->email}}</td>
            <td>{{$candidate->age}}</td>
            <td>
            
            <div class="dropdown">
                
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @if(isset($candidate->user_id))
                        {{$candidate->owner->name}}
                    @else
                        Assign owner
                    @endif
                
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                @foreach ($users as $user)
                    <a class="dropdown-item" href="{{route('candidates.changeuser',[$candidate->id, $user->id])}}">{{$user->name}}</a>
                @endforeach
                </div>
            </div>
            

            <td>
            <div class="dropdown">
        
            @if (null != App\Status::next($candidate->status_id)) 
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{$candidate->status->name}}
            </button>

                @if((App\status::next($candidate -> status_id)) != null)
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach(App\status::next($candidate -> status_id) as $status)
                            <a class="dropdown-item" href="{{route('candidates.changestatus',[$candidate->id, $status->id])}}">{{$status->name}}</a>
                        @endforeach
                    </div>
                @endif
            @else 
                {{$candidate->status->name}}
            @endif

            @if (App\Status::next($candidate->status_id) != null )
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach(App\Status::next($candidate->status_id) as $status)
                        <a class="dropdown-item" href="{{route('candidates.changestatus', [$candidate->id,$status->id])}}">{{$status->name}}</a>
                    @endforeach                               
                </div>
            @endif
        
            </div>

            </td>

            </td>
            <td>{{$candidate->created_at}}</td>
            <td>{{$candidate->updated_at}}</td>
            <td><a href="{{route('candidates.edit', $candidate->id)}}" class="btn btn-info" role="button">Edit</a></td>
            <td><a href="{{route('candidates.delete', $candidate->id)}}" class="btn btn-danger" role="button">Delete</a></td>
            <td><a href="{{route('candidates.details', $candidate->id)}}" class="btn btn-info" role="button">Details</a></td>
            
        </tr>
    @endforeach
</table>
                    
                    
@endsection
