@extends('layouts.app')

@section ('title', 'Edit candidate')

@section('content')

    <div class="jumbotron text-center">
    <h1>Edit candidate number({{$id}})</h1>
    </div>
    <form method = "post" action = "{{action('CandidatesController@update', $candidate->id)}}">
    @csrf
    <input type = "hidden" name = "_method" value = "PATCH">
    <div class="form-group">
        <label for="name">Candidate name:</label>
        <input type="text" class="form-control" placeholder="Enter name" name="name" value = {{$candidate->name}}>
    </div>
    <div class="form-group">
        <label for="email">Email:</label>
        <input type="text" class="form-control" placeholder="Enter email" name="email" value = {{$candidate->email}}>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>                 
        
@endsection
