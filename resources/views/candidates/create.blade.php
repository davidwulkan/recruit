@extends('layouts.app')

@section ('title', 'Create candidate')

@section('content')
    
    <div class="jumbotron text-center">    
    <h1>Create candidate</h1>
    </div>
    <form method = "post" action = "{{action('CandidatesController@store')}}">
    @csrf
    <div class="form-group">
      <label for="name">Candidate name:</label>
      <input type="text" class="form-control" placeholder="Enter name" name="name">
    </div>
    <div class="form-group">
      <label for="email">Email:</label>
      <input type="text" class="form-control" placeholder="Enter email" name="email">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
             
        
@endsection
