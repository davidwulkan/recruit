@extends('layouts.app')

@section ('title', 'Candidate Details')

@section('content')

    
    <h1>Candidate Details</h1>
    <p>
    ID: {{$candidate->id}}
    <p>
    Name: {{$candidate->name}}
    <p>
    Email: {{$candidate->email}}
    <p>
    Age: {{$candidate->age}}
    <p>
    Owner: {{$candidate->owner->name}}
    <p>
    <div class="dropdown">
            @if (null != App\status::next($candidate->status_id)) 
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{$candidate->status->name}}
            </button>

                @if((App\status::next($candidate -> status_id)) != null)
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach(App\status::next($candidate -> status_id) as $status)
                            <a class="dropdown-item" href="{{route('candidates.changestatus',[$candidate->id, $status->id])}}">{{$status->name}}</a>
                        @endforeach
                    </div>
                @endif
            @else 
                {{$candidate->status->name}}
            @endif

            @if (App\Status::next($candidate->status_id) != null )
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    @foreach(App\Status::next($candidate->status_id) as $status)
                        <a class="dropdown-item" href="{{route('candidates.changestatus', [$candidate->id,$status->id])}}">{{$status->name}}</a>
                    @endforeach                               
                </div>
            @endif


    

    
@endsection
