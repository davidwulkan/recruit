@extends('layouts.app')

@section ('title', 'Edit user')

@section('content')

    <div class="jumbotron text-center">
    <h1>Edit user </h1>
    </div>
    <form method = "POST" action = "{{route('auth.update', $user->id)}}">
    @csrf
    
    <div class="form-group">
        <label for="name">User name:</label>
        <input type="text" class="form-control" placeholder="Enter name" name="name" value = {{$user->name}}>
    </div>
    <div class="form-group">
        <label for="email">Email:</label>
        <input type="text" class="form-control" placeholder="Enter email" name="email" value = {{$user->email}}>
    </div>
    <div class="form-group row">
                {<label for="department_id" class="col-md-4 col-form-label text-md-right">Department</label>
                <div class="col-md-6">
                    <select class="form-control" name="department_id">                                                                         
                        @foreach ($departments as $department)
                        @if($user->department->id == $department->id)
                            <option value="{{ $department->id }}" selected="selected"> 
                                {{ $department->name }} 
                            </option>
                        @else
                        <option value="{{ $department->id }}"> 
                            {{ $department->name }} 
                        </option>
                        @endif                                      
                        @endforeach    
                        </select>
                </div>
            </div>
    <div>
    <input type = "submit" name = "submit" value = "Update user">
    </div>
                    
        
@endsection
