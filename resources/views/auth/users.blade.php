@extends('layouts.app')

@section ('title', 'Candidates')

@section('content')


<div class="jumbotron text-center">
<h1>List of Users</h1>

</div>
<table class = "table table-striped">
<thead class="thead-dark">
    <tr>
        <th>ID</th><th>Name</th><th>Email</th><th>Department</th>
    </tr>
</thead>
    <!-- the table data -->
    
    @foreach ($users as $user)
    
    <tr>
    
        
            <td>{{$user->id}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->department->name}}</td>
            <td><a href="{{route('auth.edit', $user->id)}}" class="btn btn-danger" role="button">Edit</a></td>
            <td><a href="{{route('auth.delete', $user->id)}}" class="btn btn-danger" role="button">Delete</a></td>
            <td>
            
            
            
        </tr>
    @endforeach
</table>
                    
                    
@endsection
