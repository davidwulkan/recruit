<?php

use Illuminate\Database\Seeder;

class CandidatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('candidates')->insert([
            
            'name' => Str::random(20),
            'email' => Str::random(10).'@gmail.com',
            'created_at' => date("Y-m-d H-i-s"),
            'updated_at' => date("Y-m-d H-i-s"),
            //another option
            //'created_at' => Carbon::now(),
            //'updated_at' => Carbon::now(),
            
        ]);
    }
}
