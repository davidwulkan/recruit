<?php

use Illuminate\Database\Seeder;

class statusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses=[['id' => 1, 'name' => 'before interview','created_at' => date("Y-m-d H-i-s"),'updated_at' => date("Y-m-d H-i-s")],
        ['id' => 2, 'name' => 'not fit','created_at' => date("Y-m-d H-i-s"),'updated_at' => date("Y-m-d H-i-s")],
        ['id' => 3, 'name' => 'sent to manager','created_at' => date("Y-m-d H-i-s"),'updated_at' => date("Y-m-d H-i-s")],
        ['id' => 4, 'name' => 'not fit professionally','created_at' => date("Y-m-d H-i-s"),'updated_at' => date("Y-m-d H-i-s")],
        ['id' => 5, 'name' => 'accepted to work','created_at' => date("Y-m-d H-i-s"),'updated_at' => date("Y-m-d H-i-s")]];
      
        DB::table('statuses')->insert($statuses);
    }
}
