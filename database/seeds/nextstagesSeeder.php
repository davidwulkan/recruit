<?php

use Illuminate\Database\Seeder;

class nextstagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nextstage=[['from' => 1, 'to' => 2,'created_at' => date("Y-m-d H-i-s"),'updated_at' => date("Y-m-d H-i-s")],
        ['from' => 1, 'to' => 3,'created_at' => date("Y-m-d H-i-s"),'updated_at' => date("Y-m-d H-i-s")],
        ['from' => 3, 'to' => 4,'created_at' => date("Y-m-d H-i-s"),'updated_at' => date("Y-m-d H-i-s")],
        ['from' => 3, 'to' => 5,'created_at' => date("Y-m-d H-i-s"),'updated_at' => date("Y-m-d H-i-s")]];
      
        DB::table('nextstages')->insert($nextstage);
    }
}
